﻿---
path: "/blog/on-gentrification"
date: 2019-04-27
title: "On gentrification"
author: "Camarade"
---
_V znak solidarnosti smo od tovariša iz Aten prejeli pismo na temo gentrifikacije, ki poziva k uporu proti kapitalistični politiki mest._

---

Throughout the capitalist world, former working-class neighborhoods are currently being reshaped into “smart city” dystopias, exclusively tailored for the leisure of economic elites. This process of gentrification takes various forms: the proliferation of Air BNBs and generic consumer attractions under the effects of mass tourism; the steady rise in rent and cost of living in once affordable areas; and the brute shape of evictions for those who refuse to be displaced from their homes on account of the greed of speculators. 

At the core of all three of these issues is indeed the question of displacement of population, as neighborhoods are drained of their former residents who cannot afford to thrive in this new paradigm. This destroys the very possibility of a neighborly community, based on shared history, shared spaces and experiences.

The process of replacing homes with short-term residences or services is symptomatic of a trend that perceives cities as profit-based machines that should be run as corporations: but cities are made of the people who inhabit them, and so are the neighborhoods within them. Our public spaces are not up for speculation. This neoliberal vision of “progress” is a threat to the well-being of our communities; indeed, progress can only come from the collective decision-making and action of the community towards the satisfaction of its needs.

We refuse to stand by as our homes are threatened and as our neighbors, comrades, friends face the ever-growing threat of eviction and forced displacement.
