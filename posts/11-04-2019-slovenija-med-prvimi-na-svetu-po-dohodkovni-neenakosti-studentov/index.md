﻿---
path: "/blog/slovenija-med-prvimi-na-svetu-po-dohodkovni-neenakosti-studentov"
date: 2019-04-11
title: "Slovenija med prvimi na svetu po dohodkovni neenakosti študentov"
author: "Chesterfield"
---
Nekateri študentje in študentke se vsakodnevno soočamo s težavami, kot so naš ekonomski status, težave z nastanitvijo, slabi pogoji študentskega dela in neplačane prakse. To tudi prepoznava vsakoletna raziskava Evroštudent, ki deluje pod okriljem evropskih in državnih ustanov. V nadaljevanju bom predstavil izsledke raziskave Evroštudent VI 2016–2018.

Za Slovenijo je raziskava pokazala visoko stopnjo dohodkovne neenakosti med študenti. Ta je tako visoka, da če bi bili študentje svoja država, bi bili med prvimi na svetu po dohodkovni neenakosti. Kar 38% študentov se sooča s finančnimi težavami – več jih je le v Albaniji in Gruziji. Podatek dokazuje, da smo študentje ekonomsko posebej ogrožena skupina. V socialni državi bi študij moral biti pravica za vse, ne le za nekatere. Zato je nujno, da se vsem omogoči ekonomsko varen študij.

Presenetljiv podatek je, da 48% slovenskih študentov živi pri starših ali sorodnikih ter samo 19% v subvencioniranih nastanitvah oziroma v študentskih domovih. Finska, Turčija, Nizozemska, Romunija in Slovaška imajo na primer ta odstotek med 30 in 40. Glede na to, da že zdaj zaostajamo za njimi, je zmanjšanje kapacitet v študentskih domovih seveda nesmiselno. Naš cilj bi kvečjemu moral biti povečati te kapacitete in s tem vse več študentom omogočiti prve korake k osamosvojitvi. 

Povprečen študent v Sloveniji ima manj dohodkov kot povprečen študent v EU in nameni večino teh sredstev za nastanitev in hrano. Za 70% slovenskih študentov je nastanitev največji strošek. Zavedati se moramo, da se bo ukinitve Akademskega kolegija, najcenejšega doma v Ljubljani, ta odstotek verjetno še povečal. Če se že pretvarjamo, da smo razvita država, bi morali študentom zagotoviti še kaj več kot le preživetveni minimum. Kako naj se študent osredotoči na študij, ko mora razmišljati o tem, kako si bo privoščil najemnino?
