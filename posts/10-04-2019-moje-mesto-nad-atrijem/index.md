﻿---
path: "/blog/moje-mesto-nad-atrijem"
date: 2019-04-10
title: "Moje mesto nad atrijem"
author: "Frida"
---
V študentski dom sem se vselila na začetku svojega 3. letnika. Pred tem sem se vozila od doma, vendar mi zaradi količine časa, ki ga pobere vožnja, to kasneje ni več uspevalo. Akademski kolegij sem si izbrala, ker je tu že pred mano stanoval moj fant in sva se dogovorila, da se vseliva skupaj. Nizka najemnina mi veliko pomeni, saj si jo plačujem sama. Ugodna je tudi lokacija, saj je blizu centra, železniške postaje in ima do moje fakultete direktno avtobusno povezavo. Zaradi tega menim, da Akademski kolegij s tem omogoča uspešen študij. K temu pripomorejo tudi dobri odnosi s sostanovalci ter mir v domu. 

Nastanjena sem v prostorni svetli sobi s pogledom na polkrožni atrij. Lahko sem si jo uredila po lastnem okusu. Okoli doma je veliko zelenih površin (park Navje, domski atrij) in številne trgovine.

Če nas bodo iz doma izselili, bo najemnina v novem domu vsekakor višja, saj je Akademski kolegij trenutno najcenejši študentski dom v Ljubljani. Skrbi me, da si višje najemnine ne bom mogla privoščiti. Ostali domovi v Ljubljani so po izkušnjah njihovih stanovalcev precej bolj hrupni, kar je pri študiju seveda zelo moteč dejavnik. Po za zdaj še neuradnih podatkih nas nameravajo izseliti sredi izpitnega obdobja, kar je za nas še dodatna obremenitev v že tako stresnem mesecu. 

Poleg problemov, ki zadevajo nas, študente, je tu še problem prebivalcev v socialnih stanovanjih v tem domu, ki jim Mestna občina Ljubljana ni dolžna zagotoviti nadomestnih stanovanj in se bodo po večini znašli na cesti.

Vsi, ki stanujemo v Akademskem kolegiju, smo ga vzljubili in ga želimo ohraniti v vlogi, ki jo je imel zadnjih 50 let. 
