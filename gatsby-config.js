module.exports = {
    siteMetadata: {
        title: `AK študentom!`,
        author: `Akademci`,
        siteUrl: `https://www.akademc.tk`,
        description: `Spletna stran gibanja Akademca študentom, ki se bori za bivanjske pravice študentov, specifično gre se za usodo študentskega doma Akademski Kolegij.`
    },
    
    plugins: [
        {
            resolve: "gatsby-plugin-react-svg",
            options: {
                rule: {
                    include: /icons/
                }
            }
        },
        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "posts",
                path: `${__dirname}/posts`,
            },
        },

        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "events",
                path: `${__dirname}/events`,
            },
        },
        
        {
            resolve: "gatsby-transformer-remark",
            options: {
                plugins: [
                    {
                        resolve: 'gatsby-remark-images',
                        options: {
                            maxWidth: 500,
                        },
                    },
                ],
            },
        },
        
        "gatsby-plugin-sharp",
        "gatsby-transformer-sharp",
        "gatsby-plugin-catch-links",
        "gatsby-plugin-react-helmet",

        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "images",
                path: `${__dirname}/src/images`,
            },
        },
    ],
};

