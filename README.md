stran je dostopna na www.akademc.tk

## TODO

Če hočeš strani pomagati z malo programerskega švica, si poglej odpre issue-je.

## Kako napišem post?

Post se napiše v markdown formatu (https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) in to
hopefully lepše kot je napisan ta README. Na vrhu mora biti to:

```
---
path: "/blog/naslov-posta"
date: leto-mesec-dan
title: "Naslov posta"
author: "tvoje ime"
---
```

Potem narediš v mapici posts podmapo dan-mesec-leto-naslov-posta in vanjo prekopiraš svoj post pod ime index.md.
Nato odpreš git pull request in ko ga potrdim bo na strani. Če česa od tega ne znaš, me pocukaj za rokav (namig: I like beer).

Dogodek ustvariš zelo podobno, le da mora imeti path obliko "/event/naslov-eventa" in da ne sme biti podan author (izpusti kar celo vrstico).

## Kako dodam fotke?

Rukneš jih v mapco images in narediš pull request. Če ne znaš, glej zadnjo poved v "Kako napišem post?" razdelku.
