---
path: "/event/simpozij"
date: 2019-06-13
title: "Spomini prihodnosti - 12. Kulturološki simpozij"
---
_13. in 14. junija v Akademskem kolegiju in v Avtonomni tovarni Rog._
---

Društvo Kult.co organizira že 12. Kulturološki simpozij,
tokrat solidarnostno v dveh prostorih, ki sta ogrožena zaradi
kratkogledne neoliberalne politike MOL.

**PROGRAM:**

Četrtek, 13. 6. - Avtonomna Tovarna Rog, Živko Skvotec
(Trubarjeva 72, 1000 Ljubljana)

16:20 uvodni nagovor

16:40 osmo/za o genezi osmo/ze

17:20 - 18:20 panel I
Franc Mali - Družbena distopija ali utopija: kam nas vodi prihodnji znanstveno-tehnološki razvoj
Maruša Nardoni - Kako brati tehnologijo

18:25 - 19:25 panel II
Nesa Vrečer - Jaz družbenih medijev
Martin Lipovšek - Podaljševanje življenja z znanostjo in tehnologijo

19:30 pogostitev

20:00 aftersimpozijsko spominjanje v Modrem kotu

-------------------------------------------------------------------
Petek, 14. 6. - atrij Akademskega kolegija
(Vilharjeva 13, 1000 Ljubljana)

16:00 Anka Grmek - 21. marec 2210

16:20 - 17:50 panel III
Vedran Štimac - Utopian Epistemologies
Primož Mlačnik - Identitarno gibanje: retrotopična desničarska melanholija
Natalija Majsova - Kako daleč je od kristalne krogle do Alefa? Mundaneum kot basen o dokumentih, časih in vizionarjih

18:00 - 19:00 panel IV
Tina Tomšič - Mišljenje modernizma kot vodilo prihodnosti
Janja Pečan - Odrast kot odgovor na okoljsko krizo

19:00 pogostitev

20:00 Psevdopoeti: Tom Veber, Ema Odra Raščan, Špela Setničar, Matija Sirk in Veronika Razpotnik

21:00 vsi v Roga! → Festival Roza Bagra 