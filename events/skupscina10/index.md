---
path: "/event/skupscina10"
date: 2019-07-03
title: "Kje bomo pa jutri spali? #10"
---
_Pridruži se nam na deseti skupščini, ki bo potekala 3.7. ob 19:00 v Avtonomni tovarni Rog. Dobimo se na dvorišču, v primeru slabega vremena pa se prestavimo v Živko Skvotec._
___

Smo najemniki brez varnega najema, zadolženi lastniki, brezdomci, študentje brez študentske sobe, upokojenci brez primerne oskrbe. Mi smo tisti, ki stanovanjsko krizo živimo vsak dan. 

Ker se država ne ukvarja s stanovanjsko politiko, so naša življenja odvisna od oderuških najemodajalcev in špekulantskih investitorjev. Ker so stanovanja navadno tržno blago, nimamo dostopa do primernih in dostopnih domov. 

Temu je treba reči dovolj! 

Kje bomo pa jutri spali? je odprta platforma, ki povezuje ljudi v boju za primerna in dostopna stanovanja. Gradimo odnose solidarnosti in podpore v skupnem zoperstavljanju špekulantom in pomanjkanju javne stanovanjske politike.

Naš namen je doseči potrebne strukturne in zakonodajne spremembe, ki bodo zagotavljale dostojna in dostopna stanovanja za vse. Stanovanje je osnovna človekova dobrina in ne zgolj blago v rokah tržnih špekulantov.