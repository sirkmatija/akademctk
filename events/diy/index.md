﻿---
path: "/event/diy"
date: 2019-05-16
title: "DIY dogodek"
---
_16. maj od 16h naprej v atriju Akademskega kolegija_
---

Na sproščenem ustvarjalnem dogodku bomo v atriju Akademskega kolegija ob druženju izdelovali **šablone za grafite, transparente, plakate, kolaže in fanzine**, ob tem pa bomo tudi **grafitirali in tiskali na majice**, organiziran bo tudi akustični **jam session**. S seboj prinesite:
*dekice za na travo, 
*majice in torbe, na katere bomo tiskali, 
*revije in časopise za kolaže, 
*rjuhe za transparente ter 
*glasbila za jam session.

Poskrbljeno bo za **hrano in pijačo**, ki vključuje domače pivo in koktejl bar. Zbirali bomo prostovoljne prispevke, ki bodo namenjeni Avtonomni tovarni Rog in nadaljnemu izvajanju dogodkov v Akademskem kolegiju. 

Pridi ustvarjat, podpri boj!
