﻿---
path: "/event/festival"
date: 2019-05-23
title: "Festival Akademskega kolegija"
---
_23. in 24. maja od 15h naprej v prostorih Akademskega kolegija_
---

V pripravi:
SIMPOZIJ NA TEMO GENTRIFIKACIJE · FOTOGRAFSKA RAZSTAVA AK · BRANJE POEZIJE · VEČERJA · KARAOKE · NASTOPI GLASBENIH SKUPIN · ŽUR

Namen festivala je seznaniti ljudi z Akademkim kolegijem in njegovo problematiko. Natančnejši program festivala bo v kratkem objavljen na tem mestu in na naši Facebook strani.

Med festivalom bomo v znak solidarnosti na majice tiskali logo _Akademski kolegij študentom!_, zato prinesi prazno majico in podpri Akademski kolegij!

