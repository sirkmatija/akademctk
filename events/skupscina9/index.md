---
path: "/event/skupscina9"
date: 2019-06-26
title: "Kje bomo pa jutri spali? #9"
---
_Deveta redna skupščina Kje bomo pa jutri spali?, tokrat bolj ustvarjalne narave, bo potekala v sredo, 26. 6. ob 18:00 uri v parku Zvezda na Kongresnem trgu._
---

Tokrat se premikamo v javni prostor, da razširimo in predstavimo ideje in ustvarjamo za nadaljne intervencije v javni prostor. Pridružite se nam pri izdelovanju transparentov v parku, s seboj prinesite še uporabne pripomočke (olfa nožke, barve in spreje, rjuhe in karton) in sevede lastne zgodbe o stanovanjih.

--------------------------------------------------
Kje bomo pa jutri spali? je odprta platforma, ki povezuje ljudi v boju za primerna in dostopna stanovanja.

Gradimo odnose solidarnosti in podpore v skupnem boju proti špekulantom in pomanjkanju javne politike. Naš namen je doseči potrebne strukturne in zakonodajne spremembe, ki bodo zagotavljale dostojna in dostopna stanovanja za vse. Stanovanje je osnovna človekova dobrina in ne zgolj blago v rokah tržnih špekulantov.

Pridruži se nam na rednih skupščinah, kjer razpravljamo o tegobah vsakdanjega življenja v Ljubljani in organiziramo akcije, ki nas bodo pripeljale bližje našim ciljem.