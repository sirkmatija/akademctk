import React from "react";
import Layout from "../components/layout";
import styles from "./about.module.css";

class App extends React.Component {
    render() {
        return (
            <Layout current="about">
              <h1>Kdo smo:</h1>
              
              <p>
            Skupina <b>Akademski kolegij študentom</b> predstavlja stanovalce študentskega doma Akademski kolegij. Naš cilj je ohranitev študentskega doma, saj letos njegovim stanovalcem, tako študentom kot najemnikom v neprofitnih stanovanjih, grozi izselitev. Širše pa se zavzemamo za vse študente v Ljubljani, saj trenutna politika ne predvideva rešitve stanovanjske problematike v odnosu do študentov, ekonomsko ranljivejšega dela družbe.
              </p>

              <h1>Cilji:</h1>
              
              <p>
                Za nas je ohranitev študentskega doma Akademski kolegij vsaj do konca študentske stanovanjske stiske edina smiselna rešitev. Problemi, ki jih odgovorne institucije trenutno ignorirajo, so:
                <ul>
                <li><b>Ni primerljivih nadomestnih ležišč.</b> Nadomestna ležišča, ki nam jih ŠDL ponuja, so vsa manjša, dražja in na slabši lokaciji. Tudi privatni trg ne nudi cenovno dostopnih rešitev za socialno ogrožene študente.</li>
                <li><b>Nekateri študentje še vedno čakajo na ležišče.</b> Ob začetku drugega semestra je bilo na čakalni listi še več kot 700 študentov, pri čemer se jih vsako leto vpiše več.</li>
                <li><b>Gradnja novih študentskih domov je preveč oddaljena rešitev.</b> V bližnji prihodnosti se gradnja nadomestnih kapacitet še ne bo začela, kar pomeni, da se bo stanovanjska kriza le še poslabšala.</li>
                </ul>
              </p>
              
              <h1>Zahtevamo:</h1>
              <p>
Menimo, da bi se javne ustanove, zaradi katerih nam grozi izselitev, morale zavzemati za pravice študentov v okviru stanovanjske politike. Ljubljana je univerzitetno mesto, zato odgovorne institucije pozivamo, da nam to tudi dokažejo.
              </p>

              <div id={styles.vabilo}>
                <p>
Vabimo k prebiranju vsebin na naši spletni strani, kjer bomo posamezne probleme podrobneje secirali, dali kakšni ranjeni duši glas, da se zjoče, ter obveščali o napredovanju ali nazadovanju naše situacije. Če se kdo ob povedanem kakorkoli prepozna, lepo vabljen tudi k prispevanju!
                </p>
              </div>

              <h1>Kontakt:</h1>
              <p><ul>
              <li>E-mail: <a href="mailto:ak.studentom@gmail.com">ak.studentom[at]gmail.com</a></li>
              <li>Facebook stran: <a href="https://www.facebook.com/Akademski-kolegij-%C5%A1tudentom-786628825029953/">Akademski kolegij študentom!</a></li>
              </ul></p>
            </Layout>
        );
    }
}

export default App;
