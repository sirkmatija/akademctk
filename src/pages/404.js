import React from "react";
import {Link} from "gatsby";

import LogoIcon from "../icons/logo.svg";

import styles from "./404.module.css";
console.log(styles);

export default () => (
    <div id={styles.oneErrorToRuleThemAll}>
      <LogoIcon/>
      <div>
        <h1>Ups, 404.</h1>
        <Link to="/">Glavna stran</Link>
        <p>
          Pred kratkim smo spremenili shemo naslovov objav na blogu,
          morda ste sledili stari povezavi.
        </p>

        <p>
          V tem primeru v kolikor je naslov povezave <i>"akademc.tk/naslov" </i> 
          je zdaj verjetno pravi naslov <i>"akademc.tk/blog/naslov"</i>.
        </p>
        
        <p>
          Če tudi to ne pomaga, poskusite iskano stran poiskati preko
          glavne strani.
        </p>
        
        <p>
          <b>
            Hvala!
          </b>
        </p>
      </div>
    </div>
);
