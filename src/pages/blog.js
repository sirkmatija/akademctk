import React from "react";
import { Link, graphql } from "gatsby";
import Layout from "../components/layout";

import styles from "./blog.module.css";
console.log(styles);

export default function Index({ data }) {
    const { edges: posts } = data.allMarkdownRemark;
    return (
        <Layout current="blog">
          <div id={styles.blog}>
            {posts
             .filter(post => post.node.frontmatter.title.length > 0)
             .map(({ node: post }) => {
                 return (
                     <div className={styles.preview} key={post.id}>
                       <h1>
                         <Link to={post.frontmatter.path}>{post.frontmatter.title}</Link>
                       </h1>
                       <h2>{post.frontmatter.date}</h2>
                       <p>{post.excerpt}</p>
                       <p>- {post.frontmatter.author}</p>
                     </div>
                 );
             })}
          </div>
        </Layout>
    );
}

export const pageQuery = graphql`
  query IndexQuery {
    allMarkdownRemark(filter: { frontmatter: {path: {regex: "\/blog\/"}}}, sort: { order: ASC, fields: [frontmatter___date] }) {
      edges {
        node {
          excerpt(pruneLength: 500)
          id
          frontmatter {
            title
            date(formatString: "DD. MMMM YYYY", locale: "sl_SI")
            path
            author
          }
        }
      }
    }
  }
`;
