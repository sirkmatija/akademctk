import React from "react";
import {Link} from "gatsby";

import Meta from "../components/meta";

import LogoIcon from "../icons/logo.svg";
import AboutIcon from "../icons/about.svg";
import BlogIcon from "../icons/blog.svg";
import GalleryIcon from "../icons/gallery.svg";
import TimelineIcon from "../icons/timeline.svg";

import styles from "./index.module.css";
console.log(styles);

var FancyLink = props => (
    <div className={styles.fancy}>
      <Link to={props.to}>
        {props.children}
      </Link>
    </div>
);

class App extends React.Component {
    render() {
        return (
            <div id={styles.container}> {/* needed for vertical centering. */}
              <Meta/>
              <LogoIcon className={styles.logo}/>

              <div id={styles.icons}>
                
                <FancyLink to="/about/">
                  <AboutIcon className={styles.navicon}/>
                </FancyLink>
                
                <FancyLink to="/blog/">
                  <BlogIcon className={styles.navicon}/>
                </FancyLink>
                
                <FancyLink to="/gallery/">
                  <GalleryIcon className={styles.navicon}/>
                </FancyLink>

                <FancyLink to="/timeline">
                  <TimelineIcon className={styles.navicon}/>
                </FancyLink>
              </div>
            </div>
        );
    }
}

export default App;
