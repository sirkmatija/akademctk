import React from "react";
import { Link, graphql } from "gatsby";
import Layout from "../components/layout";
import { withPrefix } from "gatsby-link";

import styles from "./timeline.module.css";


console.log(styles);

export default function TimeLine({ location, data }) {
    const { edges: events } = data.allMarkdownRemark;
    
    return (
        <Layout current="timeline">
          <div id={styles.timeline}>
            <ul>
              {events
               .filter(event => event.node.frontmatter.title.length > 0)
               .map(({ node: event }) => {
                   return (
                       <li>
                         <h1>{event.frontmatter.date}</h1>
                         <h2>
                           <Link to={event.frontmatter.path}>
                             {event.frontmatter.title}
                           </Link>
                         </h2>
                         <p>{event.excerpt}</p>
                       </li>
                   );
               })}
              <li><h2>In še več, kmalu</h2></li>
            </ul>
          </div>
        </Layout>
    );
}

export const pageQuery = graphql`
  query TimelineQuery {
    allMarkdownRemark(filter: { frontmatter: { path: {regex: "\/event\/"}}}, sort: { order: ASC, fields: [frontmatter___date] }) {
      edges {
        node {
          excerpt(pruneLength: 100)
          id
          frontmatter {
            title
            date(formatString: "DD. MMMM YYYY", locale: "sl_SI")
            path
          }
        }
      }
    }
  }
`;
