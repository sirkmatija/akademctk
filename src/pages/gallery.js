import React from "react";
import Layout from "../components/layout";
import Images from "../components/images";

export default () => (
    <div id="gallery">
      <Layout current="gallery">
        <Images/>
      </Layout>
    </div>
);
