import React from "react";
import { graphql } from "gatsby";
import Layout from "../components/layout";
import {Helmet} from "react-helmet";

import styles from "./blog-post.module.css";
console.log(styles);

export default function Template({
    data,
}) {
    const { markdownRemark: post } = data;

    return (
        <Layout current={post.frontmatter.author ? "blog" : "timeline"}>
          <Helmet>
            <meta property="og:title" content={post.frontmatter.title}/>
            <meta name="og:description" content={post.frontmatter.excerpt}/>
            <meta name="description" content={post.frontmatter.excerpt}/>
          </Helmet>
          
          <div id={styles.post}>
              <h1>{post.frontmatter.title}</h1>
            <h2>{post.frontmatter.author ? "-" + post.frontmatter.author : false}</h2>
              <div
                className="blog-post-content"
                  dangerouslySetInnerHTML={{ __html: post.html }}
              />
          </div>
        </Layout>
    );
}

export const PageQuery = graphql`
  query BlogPostByPath($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        date(formatString: "DD. MMMM YYYY", locale: "sl_SI")
        path
        title
        author
      }
    }
  }`;
