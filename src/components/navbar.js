import React from "react";
import { Link } from "gatsby";

import BlogIcon from "../icons/blog.svg";
import GalleryIcon from "../icons/gallery.svg";
import AboutIcon from "../icons/about.svg";
import LogoIcon from "../icons/logo.svg";
import TimelineIcon from "../icons/timeline.svg";

import styles from "./navbar.module.css";
console.log(styles);

class NavBar extends React.Component {
    currentPageLogo() {
        switch(this.props.current) {
        case "about":
            return <AboutIcon/>;
            break;
        case "blog":
            return <BlogIcon/>;
            break;
        case "gallery":
            return <GalleryIcon/>;
            break;
        case "timeline":
            return <TimelineIcon/>;
            break;
        default:
            return null;
        }
    }

    uncurrentLinks() {
        return (
            <div id={styles.linkovje}>
              <Link to="/about">O nas</Link>
              
              <Link to="/gallery">Galerija</Link>
              
              <Link to="/blog">Blog</Link>

              <Link to="/timeline">Dogodki</Link>
            </div>
        );
    }
    
    render() {
        return(
            <div id={styles.nav}>

              <div id={styles.current}>
                {this.currentPageLogo()}
              </div>

              <div id={styles.other}>
                {this.uncurrentLinks()}
                
                <Link to="/">
                  <LogoIcon id={styles.logo}/>
                </Link>
                <div id={styles.textmain}>
                  <Link to="/">AKŠ</Link>
                </div>
            
              </div>
            </div>
        );
    }
}

export default NavBar;
