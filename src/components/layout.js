import React from "react";
import Navbar from "./navbar";
import Meta from "./meta";

import styles from "./layout.module.css";
console.log(styles);

export default props => (
    <div id={styles.layout}>
      <Meta/>
      <Navbar current={props.current}/>
      <div id={styles.inner}>
        {props.children}
      </div>
    </div>
);
