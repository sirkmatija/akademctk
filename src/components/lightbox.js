import React, { Component } from "react";
import Img from "gatsby-image";
import PropTypes from "prop-types";

import styles from "./lightbox.module.css";
console.log(styles);

export default class Lightbox extends Component {
    static propTypes = {
        imgs: PropTypes.array.isRequired,
    }

    render() {
        const { imgs } = this.props;
        console.log(imgs);
        return (
            <div id={styles.light}>
              {imgs.map(img => (
                  <Img
                    style = {  {maxWidth: "90vw", alignSelf: "center"} }
                    imgStyle = { {maxWidth: "90vw", alignSelf: "center", padding: "1em"} }
                    key = {img.node.childImageSharp.fixed.src}
                    fixed = {img.node.childImageSharp.fixed}
                  />))}
            </div>
        );
    }
}
