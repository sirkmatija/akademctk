import React from "react";
import { StaticQuery, graphql } from "gatsby";
import Lightbox from './lightbox';

export default () => (
    <StaticQuery
      query={graphql`
        query {
          imgs:  allFile(filter: {sourceInstanceName: { eq: "images" }}) {
            edges {
              node {
                childImageSharp {
                  fixed(width: 600) {
                    ...GatsbyImageSharpFixed
                  }
                }
              }
            }
          }
        }
       `}
      render={data => <Lightbox imgs={data.imgs.edges}/>}
    />
);
