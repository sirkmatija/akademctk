import React from "react";
import {Helmet} from "react-helmet";

export default () => (
    <div id="meta">
      <Helmet>
        <meta charSet="utf-8"/>
        <title>AK študentom!</title>
        <meta name="author" content="Akademci"/>
        <meta name="description" content="Spletna stran gibanja Akademca študentom, ki se bori za bivanjske pravice študentov, specifično gre se za usodo študentskega doma Akademski Kolegij."/>
        <meta name="keywords" content="Študenti, AK, Akademski, Kolegij, Študentski, Dom, ŠDLJ, MOL"/>
        
      </Helmet>
    </div>
)
